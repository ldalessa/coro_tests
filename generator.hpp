// Work done by https://github.com/lewissbaker, copied from https://godbolt.org/z/Ip_GmJ.
#pragma once

#if __has_include(<coroutine>)
#include <coroutine>
#else
#include <experimental/coroutine>
namespace std {
using std::experimental::suspend_always;
using std::experimental::suspend_never;
using std::experimental::noop_coroutine;
using std::experimental::noop_coroutine_handle;
using std::experimental::coroutine_handle;
}
#endif

#include <type_traits>
#include <iterator>
#include <cstdio>
#include <exception>
#include <utility>
#include <cassert>

namespace detail
{

template <typename T>
class manual_lifetime {
 public:
  manual_lifetime() noexcept {}
  ~manual_lifetime() {}

  template <typename... Args>
  T& construct(Args&&... args) noexcept(
      std::is_nothrow_constructible_v<T, Args...>) {
    return *::new (static_cast<void*>(std::addressof(value_)))
    T((Args &&) args...);
  }

  void destruct() noexcept(std::is_nothrow_destructible_v<T>) {
    value_.~T();
  }

  T& get() & noexcept {
    return value_;
  }
  T&& get() && noexcept {
    return (T &&) value_;
  }
  const T& get() const& noexcept {
    return value_;
  }
  const T&& get() const&& noexcept {
    return (const T&&)value_;
  }

 private:
  union {
    T value_;
  };
};

template <typename T>
class manual_lifetime<T&> {
 public:
  manual_lifetime() noexcept : value_(nullptr) {}
  ~manual_lifetime() {}

  T& construct(T& value) noexcept {
    value_ = std::addressof(value);
    return value;
  }

  void destruct() noexcept {}

  T& get() const noexcept {
    return *value_;
  }

 private:
  T* value_;
};

template <typename T>
class manual_lifetime<T&&> {
 public:
  manual_lifetime() noexcept : value_(nullptr) {}
  ~manual_lifetime() {}

  T&& construct(T&& value) noexcept {
    value_ = std::addressof(value);
    return (T &&) value;
  }

  void destruct() noexcept {}

  T&& get() const noexcept {
    return (T &&) * value_;
  }

 private:
  T* value_;
};

template <>
class manual_lifetime<void> {
 public:
  manual_lifetime() noexcept = default;
  ~manual_lifetime() = default;

  void construct() noexcept {}
  void destruct() noexcept {}
  void get() const noexcept {}
};

}

template<typename Ref, typename Value = std::remove_cvref_t<Ref>>
class generator {
 public:
  class promise_type {
    using reference = Ref;
   public:
    generator get_return_object() noexcept {
      auto h = std::coroutine_handle<promise_type>::from_promise(*this);
      rootOrLeaf_ = h;
      return generator{h};
    }

    void unhandled_exception() {
      if (exception_ != nullptr) {
        *exception_ = std::current_exception();
      } else {
        throw;
      }
    }

    void return_void() noexcept {}

    std::suspend_always initial_suspend() noexcept { return {}; }

    struct final_awaiter {
      bool await_ready() noexcept {
        return false;
      }

      std::coroutine_handle<> await_suspend(
          std::coroutine_handle<promise_type> h) noexcept {
        auto& promise = h.promise();
        auto parent = h.promise().parent_;
        if (parent) {
          promise.rootOrLeaf_.promise().rootOrLeaf_ = parent;
          return parent;
        }
        return std::noop_coroutine();
      }

      void await_resume() noexcept {}
    };

    final_awaiter final_suspend() noexcept {
      return {};
    }

    std::suspend_always yield_value(reference&& x)
      noexcept(std::is_nothrow_move_constructible_v<reference>) {
      rootOrLeaf_.promise().value_.construct(static_cast<reference&&>(x));
      return {};
    }

    template<typename T>
    requires
    (!std::is_reference_v<reference>) &&
    std::is_convertible_v<T, reference>
    std::suspend_always yield_value(T&& value)
      noexcept(std::is_nothrow_constructible_v<reference, T>) {
      rootOrLeaf_.promise().value_.construct(static_cast<T&&>(value));
      return {};
    }

    struct yield_sequence_awaiter {
      generator gen_;
      std::exception_ptr exception_;

      explicit yield_sequence_awaiter(generator&& g) noexcept
          : gen_(static_cast<generator&&>(g))
      {}

      bool await_ready() noexcept {
        return !gen_.coro_;
      }

      std::coroutine_handle<> await_suspend(
          std::coroutine_handle<promise_type> h) noexcept {
        auto& thisPromise = h.promise();
        auto& rootPromise = thisPromise.rootOrLeaf_.promise();
        auto& nestedPromise = gen_.coro_.promise();
        nestedPromise.rootOrLeaf_ = thisPromise.rootOrLeaf_;
        nestedPromise.parent_ = h;
        nestedPromise.exception_ = &exception_;
        rootPromise.rootOrLeaf_ = gen_.coro_;
        return gen_.coro_;
      }

      void await_resume() {
        if (exception_) {
          std::rethrow_exception(std::move(exception_));
        }
      }
    };

    yield_sequence_awaiter yield_value(generator&& g) noexcept {
      return yield_sequence_awaiter{std::move(g)};
    }

    // Disable use of co_await within this coroutine.
    void await_transform() = delete;

   private:
    friend generator;

    std::coroutine_handle<promise_type> rootOrLeaf_;
    std::coroutine_handle<promise_type> parent_;
    std::exception_ptr* exception_ = nullptr;
    detail::manual_lifetime<std::remove_cv_t<reference>> value_;
  };

  generator() noexcept = default;

  generator(generator&& other) noexcept
      : coro_(std::exchange(other.coro_, {}))
      , started_(std::exchange(other.started_, false))
  {}

  ~generator() {
    if (coro_) {
      if (started_ && !coro_.done()) {
        coro_.promise().value_.destruct();
      }
      coro_.destroy();
    }
  }

  generator& operator=(generator g) noexcept {
    swap(g);
    return *this;
  }

  void swap(generator& other) noexcept {
    std::swap(coro_, other.coro_);
  }

  struct sentinel {};

  class iterator {
    using coroutine_handle = std::coroutine_handle<promise_type>;

   public:
    using iterator_category = std::input_iterator_tag;
    using difference_type = std::ptrdiff_t;
    using value_type = Value;
    using reference =  Ref;
    using pointer =    std::add_pointer_t<Ref>;

    iterator() noexcept = default;
    iterator(const iterator &) = delete;
    iterator(iterator && o) {
      std::swap(coro_, o.coro_);
    }

    iterator &operator=(iterator &&) {
      return *this;
    }

    ~iterator() {}

    friend bool operator==(const iterator &it, sentinel) noexcept {
      return !it.coro_ || it.coro_.done();
    }

    iterator &operator++() {
      assert(coro_);
      assert(!coro_.done());
      coro_.promise().value_.destruct();
      coro_.promise().rootOrLeaf_.resume();
      return *this;
    }

    void operator++(int) {
      (void)operator++();
    }

    reference operator*() const noexcept {
      return coro_.promise().value_.get();
    }

    pointer operator->() const noexcept
      requires std::is_reference_v<reference> {
      return std::addressof(coro_.promise().value_.get());
    }

   private:
    friend generator;

    explicit iterator(coroutine_handle coro) noexcept
        : coro_(coro) {}

    coroutine_handle coro_;
  };

  iterator begin() {
    if (coro_) {
      started_ = true;
      coro_.resume();
    }
    return iterator{coro_};
  }

  sentinel end() noexcept {
    return {};
  }

 private:
  explicit generator(std::coroutine_handle<promise_type> coro) noexcept
      : coro_(coro)
  {}

  std::coroutine_handle<promise_type> coro_;
  bool started_ = false;
};
