static constexpr const char USAGE[] =
 R"(perf: Simple coroutine generator performance test
  Usage:
      perf (-h | --help)
      perf --version
      perf [--lambda] [--gen] [--genrec] [--range] BOUNDS...

  Options:
      -h, --help            show this screen
      --version             version information
      --lambda
      --gen
      --genrec
      --range
)";

#include "generator.hpp"
#include <cppcoro/recursive_generator.hpp>
#include <docopt.h>
#include <array>
#include <chrono>
#include <map>
#include <iostream>
#include <tuple>

namespace {
/// Grab a column from the NxM matrix `A`.
template <std::size_t n, typename T, std::size_t M, std::size_t N>
inline constexpr std::array<T, M> column(std::array<std::array<T, N>, M> A) noexcept {
  return std::apply([]<typename... Row>(Row&&... row) {
    return std::array{std::forward<Row>(row)[n]...};
  }, A);
}

/// Apply `op` to all of the N-dimensional indices in the slice.
template <typename T, std::size_t N, typename Op, typename... Is>
void dense_lambda(std::array<std::array<T, 2>, N> bounds, Op&& op, Is... is) {
  constexpr std::size_t n = sizeof...(Is);
  if constexpr (n < N) {
    for (T i = bounds[n][0], e = bounds[n][1]; i < e; ++i) {
      dense_lambda(bounds, op, is..., i);
    }
  }
  else {
    op(is...);
  }
}

/// Generate all of the N-dimensional indices in the slice, recursively.
template <typename T, std::size_t N, typename... Is>
cppcoro::recursive_generator<const std::array<T, N>>
dense_generator_recursive(std::array<std::array<T, 2>, N> bounds, Is... is) {
  constexpr std::size_t n = sizeof...(Is);
  if constexpr (n < N) {
    for (T i = bounds[n][0], e = bounds[n][1]; i != e; ++i) {
      co_yield dense_generator_recursive(bounds, is..., i);
    }
  }
  else {
    co_yield { is... };
  }
}

/// Generate all of the N-dimensional indices in the slice.
template <typename T, std::size_t N>
generator<const std::array<T, N>>
dense_generator(std::array<std::array<T, 2>, N> bounds) {
  // carry-sum increment, decrement n to make order match, saturate at upper bound
  auto inc = [&](std::array<T, N>& i) {
    for (std::size_t n = N; n > 0; --n) {
      if (++i[n-1] < bounds[n-1][1]) {
        return;
      }
      i[n-1] = bounds[n-1][0];
    }
    i = column<1>(bounds);
  };

  for (std::array i = column<0>(bounds), e = column<1>(bounds); i != e; inc(i)) {
    co_yield i;
  }
}

/// Enumerate all of the N-dimensional indices in the slice.
template <typename T, std::size_t N>
struct dense_range {
  std::array<std::array<T, 2>, N> bounds;

  explicit dense_range(std::array<std::array<T, 2>, N> bounds) noexcept : bounds(bounds) {
  }

  struct iterator {
    std::array<std::array<T, 2>, N> bounds;
    std::array<T, N> i;

    std::array<T, N> operator*() noexcept { return i; };

    iterator& operator++() noexcept {
      for (std::size_t n = N; n > 0; --n) {
        if (++i[n-1] < bounds[n-1][1]) {
          return *this;
        }
        i[n-1] = bounds[n-1][0];
      }
      i = column<1>(bounds);                    // so that end checks work
      return *this;
    }

    bool operator!=(const iterator& b) const noexcept {
      return i != b.i;
    }
  };

  iterator begin() { return { bounds, column<0>(bounds) }; }
  iterator   end() { return { bounds, column<1>(bounds) }; }
};

template <typename... Is>
[[gnu::noinline]] void print(Is... is) {
  int i = 0;
  ((std::cout << ((i++) ? "," : "") << is), ..., (std::cout << "\n"));
}

template <typename T, std::size_t N>
[[gnu::noinline]] void test_dense_lambda(std::array<std::array<T, 2>, N> bounds) {
  dense_lambda(bounds, [](auto... is) {
    print(is...);
  });
}

template <typename T, std::size_t N>
[[gnu::noinline]] void test_dense_generator_recursive(std::array<std::array<T, 2>, N> bounds) {
  for (auto&& i : dense_generator_recursive(bounds)) {
    std::apply([](auto... is) {
      print(is...);
    }, i);
  }
}

template <typename T, std::size_t N>
[[gnu::noinline]] void test_dense_range(std::array<std::array<T, 2>, N> bounds) {
  for (auto&& i : dense_range(bounds)) {
    std::apply([](auto... is) {
      print(is...);
    }, i);
  }
}

template <typename T, std::size_t N>
[[gnu::noinline]] void test_dense_generator(std::array<std::array<T, 2>, N> bounds) {
  for (auto&& i : dense_generator(bounds)) {
    std::apply([](auto... is) {
      print(is...);
    }, i);
  }
}

template <std::size_t N>
std::array<std::array<int, 2>, N> parse_bounds(std::vector<std::string> strings)
{
  std::array<std::array<int, 2>, N> b;
  std::size_t i = 0;
  for (auto&& str : strings) {
    b[i/2][i%2] = std::stol(str);
    ++i;
  }
  return b;
}

template <class Op>
double time_op(Op&& op)
{
  auto start = std::chrono::high_resolution_clock::now();
  op();
  std::chrono::duration<double> end = std::chrono::high_resolution_clock::now() - start;
  return end.count();
}

template <std::size_t N, typename Args>
int run(Args&& args) {
  std::array bounds = parse_bounds<N>(args["BOUNDS"].asStringList());
  for (std::size_t n = 0; n < N; ++n) {
    std::cout << n << ": " << bounds[n][0] << "," << bounds[n][1] << "\n";
  }

  std::map<std::string, double> times;

  if (args["--gen"].asBool()) {
    std::cout << "dense generator\n";
    times["gen"] = time_op([=] { test_dense_generator(bounds); });
  }

  if (args["--lambda"].asBool()) {
    std::cout << "dense lambda\n";
    times["lambda"] = time_op([=] { test_dense_lambda(bounds); });
  }

  if (args["--genrec"].asBool()) {
    std::cout << "dense generator recursive\n";
    test_dense_generator_recursive(bounds);
    times["genrec"] = time_op([=] { test_dense_generator_recursive(bounds); });
  }

  if (args["--range"].asBool()) {
    std::cout << "dense range\n";
    times["range"] = time_op([=] { test_dense_range(bounds); });
  };

  for (auto&& [str, time] : times) {
    std::cerr << str << " " << time << "\n";
  }

  return 0;
}
}

int main(int argc, char* const argv[]) {
  std::vector<std::string> strings(argv + 1, argv + argc);
  auto args = docopt::docopt(USAGE, strings, true);

  switch (args["BOUNDS"].asStringList().size()) {
   case 2: return run<1>(args);
   case 4: return run<2>(args);
   case 6: return run<3>(args);
   case 8: return run<4>(args);
   case 10: return run<5>(args);
   case 12: return run<6>(args);
   default: return 1;
  };
}
